# Ticket4Sale

## Requirements

- Java 11
- Scala 2.13
- sbt 1.3.x

## Running from sources

To run an application download project from https://bitbucket.org/tnaumova/tickets4sale.

### CLI tool

```
   sbt "runMain com.ticket4sale.MainApp"
```
### Web interface
List of the shows expected to be in the root directory of the project in file named 'show.csv'.

```
   sbt "run %PORT%"
```

If %PORT% is not provided application will be available at [http://localhost:9000].

## Running binary

Download 'ticket4sale-1.0.zip' from 'Downloads' section from BitBucket, unpack it into any directory. Start console int root directory.

### CLI tool

```
   ./bin/ticket4sale-cli
```

### Web interface

List of the shows expected to be in the root in file named 'show.csv'.

```
   ./bin/ticket4sale
```

Application will be available at [http://localhost:9000].
