package controllers

import com.ticket4sale.Inventory._
import com.ticket4sale.WebInventory
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import javax.inject.{Inject, Singleton}
import scala.util.{Failure, Success, Try}

/**
 * Main controller for web interface
 */
@Singleton
class Application @Inject()(val controllerComponents: ControllerComponents)
                           (implicit webJarsUtil: org.webjars.play.WebJarsUtil)
  extends BaseController {

  val inventory: WebInventory = WebInventory()

  def index: Action[AnyContent] = Action {
    Ok(views.html.index(Map(), "", ""))
  }

  def shows(showDate: String): Action[AnyContent] = Action {
    Try {
      val date = DATE_FORMAT.parseDateTime(showDate)
      inventory.ticketsByGenreWithPrice(date)
    } match {
      case Success(ticketsInfo) =>
        Ok(views.html.index(ticketsInfo, showDate, ""))
      case Failure(e) =>
        Ok(views.html.index(Map(), showDate, s"Failed to get list of shows $e"))
    }

  }
}
