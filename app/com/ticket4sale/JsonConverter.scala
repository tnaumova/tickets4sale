package com.ticket4sale

import com.ticket4sale.Show.{TicketsInfo, Genre}
import play.api.libs.json._

/**
 *  Contains json writes, to serialize inventory to json
 */
object JsonConverter {

  implicit val inventoryWrites = new Writes[Map[Genre, List[TicketsInfo]]] {
    def writes(ticketsInfo: Map[Genre, List[TicketsInfo]]) = Json.obj(
      "inventory" -> ticketsInfo.map {
        case (genre: Genre, info: List[TicketsInfo]) => Json.obj(
          "genre" -> genre.name.toLowerCase,
          "shows" -> info.map(ticketsStatus))
      }
    )
   }

  private def ticketsStatus(input: TicketsInfo):JsValue = {
    Json.obj(
      "title" -> input.showTitle.toLowerCase,
      "tickets left" -> input.ticketsLeft,
      "tickets available" -> input.ticketsAvailable,
      "status" -> input.ticketStatus.toLowerCase
    )
  }

}
