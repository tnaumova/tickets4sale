package com.ticket4sale

import play.api.libs.json.Json
import Inventory._
import JsonConverter._

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

/**
 * Main class, represents CLI tool to request inventory of tickets
 */
object MainApp extends  App {

  val date = """(\d\d\d\d)-(\d\d)-(\d\d)""".r

  def printHelp(): Unit = {
    println(
      """
        | Welcome to tickets4sale CLI. Supported commands are
        |     load %FILE_NAME%                      to upload shows list
        |     query %SHOW_DATE% %QUERY_DATE%        to request shows inventory, dates should be 'yyyy-MM-dd'
        |     quit                                  to exit program
        |     help                                  to print help""".stripMargin)
  }

  @tailrec
  def execute(inventory: Inventory): Unit = {
    println("Please enter next command")
    val line = scala.io.StdIn.readLine()
    line.split(" ") match {
      case Array("quit") => System.exit(0)
      case Array("help") =>
        printHelp()
        execute(inventory)
      case Array("load", fileName) =>
        println(s"Loading $fileName...")
        Try {
          Inventory(fileName)
        } match {
          case Success(_) =>
            val newInventory = Inventory(fileName)
            println(s"Available ${newInventory.shows.length} show(s) to query")
            execute(newInventory)
          case Failure(e) =>
            println(s"Failed to load file '$fileName' caused by $e")
            execute(inventory)

        }
      case Array("query", showDate @ date(_*), queryDate @ date(_*)) =>
        val json = Json.toJson(inventory.ticketsByGenre(DATE_FORMAT.parseDateTime(showDate),
          DATE_FORMAT.parseDateTime(queryDate)))
        println(Json.prettyPrint(json))
        execute(inventory)
      case _ =>
        println(s"Invalid command '$line'")
        execute(inventory)
    }
  }
  // Executed at start up
  printHelp()
  execute(Inventory(List()))
}
