package com.ticket4sale

import org.joda.time.DateTime

/**
 * Web model object
 */
object WebInventory {

  def apply(): WebInventory = {
    new WebInventory(Inventory("shows.csv"))
  }

}
class WebInventory(inventory: Inventory) {

  def ticketsByGenreWithPrice(showDate: DateTime): Map[String, List[List[Any]]] = {
    val queryDate = DateTime.now.withTimeAtStartOfDay()
    inventory.ticketsByGenre(showDate, queryDate) .map {
      case (genre, shows) =>(genre.name, shows.map(
        ticketsInfo =>
          List(ticketsInfo.showTitle, ticketsInfo.ticketsLeft, ticketsInfo.ticketsAvailable,
            ticketsInfo.ticketStatus, ticketsInfo.price)))}
  }

}
