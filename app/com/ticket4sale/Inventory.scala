package com.ticket4sale

import java.nio.file.{Files, Paths}
import com.github.tototoshi.csv.CSVReader
import com.ticket4sale.Show.{Genre, TicketsInfo}
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}


/**
 * Parses input file into list of shows allows to request inventory
 */
object Inventory {

  val DATE_FORMAT: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def apply(filePath: String): Inventory = {
    val path = Paths.get(filePath)

    if (!Files.isRegularFile(path)) {
      throw new IllegalArgumentException(s"$path should be valid file.")
    }

    val shows = CSVReader.open(filePath).all.map {
      case List(name: String, startDate: String, genre : String) =>
        new Show(DATE_FORMAT.parseDateTime(startDate), name, Genre(genre))
      case row =>
        throw new IllegalStateException(s"Cannot parse row $row")
    }
    
    new Inventory(shows)
  }

  def apply(shows: List[Show]): Inventory = {
    new Inventory(shows)
  }

}

class Inventory(val shows: List[Show]) {

  def ticketsByGenre(showDate: DateTime, queryDate: DateTime): Map[Genre, List[TicketsInfo]] = {
    shows
      .filter(_.hasShowOnDate(showDate))
      .groupBy(_.genre)
      .view.mapValues(_.map(_.ticketsInfo(showDate, queryDate))).toMap
  }
}
