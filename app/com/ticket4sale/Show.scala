package com.ticket4sale

import com.github.nscala_time.time.Imports
import com.ticket4sale.Show._
import com.github.nscala_time.time.Imports._
import org.joda.time.Days._

object Show {

  object Genre {
    def apply(name : String): Genre = name.toLowerCase match {
      case "comedy" => Comedy
      case "musical" => Musical
      case "drama" => Drama
      case _ => throw new IllegalArgumentException(s"Unknown genre $name")
    }
  }
  case class Genre(name: String, price: Double)
  val Comedy = Genre("Comedy", 50)
  val Musical = Genre("Musical", 70)
  val Drama = Genre("Drama", 40)

  case class TicketsStatus(name: String)
  val OpenForSale: TicketsStatus = TicketsStatus("open for sale")
  val SoldOut = TicketsStatus("sold out")
  val SaleNotStarted = TicketsStatus("sale not started")
  val InThePast = TicketsStatus("in the past")

  case class TicketsInfo(showTitle: String, ticketsAvailable: Int, ticketsLeft: Int, ticketStatus: String, price: Double)
}

case class Show(startDate: DateTime, title: String,  genre: Genre) {

  val sellStarts = 25
  val soldOutBefore = 5
  val totalDays = 100

  val lastDayInBigHall = 60

  val ticketsPerDayInBigHall = 10
  val ticketsPerDayInSmallHall = 5

  val bigHallCapacity = 200
  val smallHallCapacity = 100

  val discountStartsAfter = 80

  def ticketsLeft(showDate: DateTime, queryDate: DateTime): Int = {
    val days = daysSinceOpening(showDate)
    val sellOpenedFor = daysBetween(showDate minusDays sellStarts, queryDate).getDays
    if (sellOpenedFor > sellStarts - soldOutBefore) {
      0
    } else if (days <= lastDayInBigHall ) {
      calculateTicketsLeft(bigHallCapacity, sellOpenedFor, ticketsPerDayInBigHall)
    } else {
      calculateTicketsLeft(smallHallCapacity, sellOpenedFor, ticketsPerDayInSmallHall)
    }

  }

  private def calculateTicketsLeft(capacity: Int, daysOnSale: Int, ticketsPerDay: Int) = {
    if (daysOnSale <= 0 ) {
      capacity
    } else {
      capacity - (daysOnSale -1) * ticketsPerDay
    }
  }

  def hasShowOnDate(showDate : DateTime): Boolean = {
    val days = daysSinceOpening(showDate)
    days >= 0 && days <= totalDays
  }

  def ticketsAvailable(showDate: DateTime, queryDate: DateTime): Int = {
    val days = daysBetween(startDate, showDate).getDays
    if (!isSellStarted(showDate, queryDate)|| daysBetween(showDate, queryDate).getDays >= -soldOutBefore) {
      0
    } else if (days <= lastDayInBigHall ) {
      ticketsPerDayInBigHall
    } else {
      ticketsPerDayInSmallHall
    }
  }

  private def isSellStarted(showDate: Imports.DateTime, queryDate: Imports.DateTime): Boolean = {
    daysBetween(showDate, queryDate).getDays >= -sellStarts
  }

  def calculatePrice(showDate: DateTime): Double = {
      if (daysSinceOpening(showDate) > discountStartsAfter) {
        genre.price * 0.8
      } else {
        genre.price
      }
  }

  def ticketsStatus(showDate: DateTime, queryDate: DateTime): TicketsStatus = {
    if (daysSinceOpening(showDate) > totalDays) { InThePast
    } else {
      daysBetween(queryDate, showDate).getDays match {
        case days if days > sellStarts => SaleNotStarted
        case days if days < soldOutBefore && days >= 0 => SoldOut
        case days if days < 0 => InThePast
        case _ => OpenForSale
      }
    }
  }

  def ticketsInfo(showDate: DateTime, queryDate: DateTime): TicketsInfo = TicketsInfo(
    title,
    ticketsAvailable(showDate, queryDate),
    ticketsLeft(showDate, queryDate),
    ticketsStatus(showDate, queryDate).name,
    calculatePrice(showDate)
  )

  def daysSinceOpening(showDate: DateTime): Int = {
    daysBetween(startDate, showDate).getDays
  }

}
