lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := "tickets4sale",
    version := "0.1",
    scalaVersion     := "2.13.6" ,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.10" % Test,
      "com.github.nscala-time" %% "nscala-time" % "2.30.0",
      "com.github.tototoshi" %% "scala-csv" % "1.3.8",
      "org.webjars" %% "webjars-play" % "2.8.8",
      "org.webjars" % "bootstrap" % "3.3.7",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
      guice
    )
  )

mainClass := Some("com.ticket4sale.MainApp")
