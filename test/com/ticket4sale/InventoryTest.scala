package com.ticket4sale

import com.ticket4sale.Inventory._
import com.ticket4sale.Show._
import org.scalatest._
import matchers.should.Matchers._
import org.scalatest.flatspec.AnyFlatSpec

/**
 * Contains tests for Inventory
 */
class InventoryTest extends AnyFlatSpec {

  "When file path is not a file" should " throw an exception" in {
    intercept[IllegalArgumentException] {
      Inventory("/")
    }
  }

  "When shows_4.csv is passed" should "return list of shows" in {
    Inventory("test/resources/shows_4.csv").shows should have length 4
  }

  "When shows_4.csv is passed" should "parse row into list of shows" in {
    val shows = Inventory("test/resources/shows_4.csv").shows
    shows should contain(new Show(DATE_FORMAT.parseDateTime("2015-02-14"), "1984", Drama))
    shows should contain(new Show(DATE_FORMAT.parseDateTime("2015-03-10"), "39 STEPS, THE ", Comedy))
    shows should contain(new Show(DATE_FORMAT.parseDateTime("2015-08-28"), "A MIDSUMMER NIGHT’S DREAM - IN NEW ORLEANS", Drama))
    shows should contain(new Show(DATE_FORMAT.parseDateTime("2015-07-11"), "ANNIE JR", Musical))
  }

  "When requested inventory for list of Shows" should "return list of tickets info" in {
    val showDate = DATE_FORMAT.parseDateTime("2015-07-01")
    val queryDate = DATE_FORMAT.parseDateTime("2015-01-01")
    Inventory(List[Show]()).ticketsByGenre(showDate, queryDate) shouldEqual Map()

    val tickets = Inventory("test/resources/shows_3.csv").ticketsByGenre(showDate, queryDate)

    tickets should contain key Musical
    tickets should contain key Comedy
    tickets should not contain key(Drama)

    tickets.getOrElse(Musical, List()) should contain(TicketsInfo("Cats", 0, 200, "sale not started", Musical.price))
    tickets.getOrElse(Comedy, List()) should contain(TicketsInfo("Comedy of Errors", 0, 200, "sale not started", Comedy.price))
  }

  "When requested inventory for list of Shows" should " should match values in scenario 2" in {
    val showDate = DATE_FORMAT.parseDateTime("2015-08-15")
    val queryDate = DATE_FORMAT.parseDateTime("2015-08-01")

    val tickets = Inventory("test/resources/shows_3.csv").ticketsByGenre(showDate, queryDate)

    tickets should contain key Musical
    tickets should contain key Comedy
    tickets should contain key Drama

    tickets.getOrElse(Musical, List()) should contain(TicketsInfo("Cats", 5, 50, "open for sale", Musical.price))
    tickets.getOrElse(Comedy, List()) should contain(TicketsInfo("Comedy of Errors", 10, 100, "open for sale", Comedy.price))
    tickets.getOrElse(Drama, List()) should contain(TicketsInfo("Everyman", 10, 100, "open for sale", Drama.price))
  }


}
