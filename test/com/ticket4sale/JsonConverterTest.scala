package com.ticket4sale

import com.ticket4sale.Inventory._
import com.ticket4sale.JsonConverter._
import play.api.libs.json.Json
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class JsonConverterTest extends AnyFlatSpec {

  "When passed list of Shows inventoryJSON" should "return JSON of inventory" in {
    val showDate = DATE_FORMAT.parseDateTime("2015-08-15")
    val queryDate = DATE_FORMAT.parseDateTime("2015-08-01")
    Inventory(List[Show]()).ticketsByGenre(showDate, queryDate) shouldEqual Map()

    val expected = Json.parse("""{"inventory": [
                 |  {"genre": "musical", "shows":[
                 |    {"title": "cats", "tickets left": 50, "tickets available": 5, "status": "open for sale"}
                 |  ]},
                 |  {"genre": "comedy", "shows":[
                 |    {"title": "comedy of errors", "tickets left": 100, "tickets available": 10,
                 |"status": "open for sale"}
                 |  ]},
                 |  {"genre": "drama", "shows":[
                 |    {"title": "everyman", "tickets left": 100, "tickets available": 10, "status":
                 |"open for sale"}
                 |]} ]}""".stripMargin)

    Json.toJson(Inventory("test/resources/shows_3.csv").ticketsByGenre(showDate, queryDate)).toString() shouldEqual expected

  }

}
