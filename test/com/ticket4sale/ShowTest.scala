package com.ticket4sale

import com.ticket4sale.Show._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._


class ShowTest extends AnyFlatSpec {

  val startDate = DateTime.now()
  val show = new Show(startDate, "Taras Bulba", Drama)
  val showInSmallHall = startDate plusDays 61
  val showInBigHall = startDate plusDays 10
  val comedy = new Show(startDate, "DEAR LUPIN", Comedy)
  val musical = new Show(startDate, "Cats", Musical)

  "When show is in small hall" should " calculate tickets including number days tickets are open for sale" in {
    show.ticketsLeft(showInSmallHall, showInSmallHall minusDays 14) shouldBe 50
    show.ticketsLeft(showInSmallHall, showInSmallHall minusDays 13) shouldBe 45
    show.ticketsLeft(showInSmallHall, showInSmallHall minusDays 10) shouldBe 100 - 14*5
    show.ticketsLeft(showInSmallHall, showInSmallHall minusDays 25) shouldBe 100
    show.ticketsLeft(showInSmallHall, showInSmallHall minusDays 24) shouldBe 100
  }

  "When show is in big hall" should " calculate tickets including number days tickets are open for sale" in {
    show ticketsLeft(startDate, startDate minusDays 10) shouldBe 200 - 14*10
    show ticketsLeft(startDate, startDate minusDays 15) shouldBe 200 - 9*10
    show.ticketsLeft(showInBigHall, showInBigHall minusDays 24) shouldBe 200
    show ticketsLeft(startDate, startDate minusDays 25) shouldBe 200
    show ticketsLeft(startDate, startDate minusDays 20) shouldBe 200 - 4*10
  }

  "When show is in the small hall and tickets not sold out" should "return always to available" in {
    show.ticketsAvailable(showInSmallHall, showInSmallHall minusDays 6) shouldBe 5
    show.ticketsAvailable(showInSmallHall, showInSmallHall minusDays 15) shouldBe 5
    show.ticketsAvailable(showInSmallHall, showInSmallHall minusDays 20) shouldBe 5
    show.ticketsAvailable(showInSmallHall, showInSmallHall minusDays 25) shouldBe 5
  }

  "Show" should "return correct value is show is on stage for particular date" in {
    show hasShowOnDate startDate shouldEqual true
    show hasShowOnDate(startDate minusDays 10) shouldEqual false
    show hasShowOnDate(startDate plusDays 10) shouldEqual true
    show hasShowOnDate(startDate plusDays 100) shouldEqual true
    show hasShowOnDate(startDate plusDays 101) shouldEqual false
  }

  "When show in the big hall and tickets not sold out" should "return always to available" in {
    show ticketsAvailable(startDate, startDate minusDays 20) shouldBe 10
    show.ticketsAvailable(showInBigHall, showInBigHall minusDays 25) shouldBe 10
    show.ticketsAvailable(startDate plusDays 60, startDate plusDays  40) shouldBe 10
  }

  "When tickets sold out" should "return 0 tickets available" in {
    show.ticketsLeft(showInBigHall, showInBigHall minusDays 4) shouldBe 0
    show.ticketsAvailable(showInBigHall, showInBigHall minusDays 4) shouldBe 0
  }

  "On the show date" should " be no tickets left and available" in {
    val showDate = startDate plusDays  5
    show.ticketsLeft(showDate, showDate) shouldBe 0
    show.ticketsAvailable(showDate, showDate) shouldBe 0
  }

  "Days since opening" should "be calculated correctly" in {
    show daysSinceOpening startDate shouldBe 0
    show daysSinceOpening(startDate plusDays 5) shouldBe 5
    show daysSinceOpening(startDate minusDays 5) shouldBe -5
  }

  "Tickets status" should "be correct" in {
    show ticketsStatus(startDate, startDate minusDays 25) shouldBe OpenForSale
    show ticketsStatus(startDate, startDate minusDays 20) shouldBe OpenForSale
    show ticketsStatus(startDate, startDate minusDays 20) shouldBe OpenForSale
    show ticketsStatus(startDate, startDate minusDays 4) shouldBe SoldOut
    show ticketsStatus(startDate, startDate) shouldBe SoldOut
    show ticketsStatus(startDate plusDays 101, startDate plusDays 80) shouldBe InThePast
    show ticketsStatus(startDate, startDate plusDays 101) shouldBe InThePast
  }

  "Show status" should "return correct values for left tickets" in {
    show ticketsInfo(startDate, startDate minusDays 19) shouldBe TicketsInfo("Taras Bulba", 10, 150, "open for sale", Drama.price)
  }

  "When test cases from description" should "return correct values" in {
    val format = DateTimeFormat.forPattern("yyyy-MM-dd")
    val cats = Show(format.parseDateTime("2022-06-01"), "Cats", Genre("musical"))
    val showDate = format.parseDateTime("2022-07-01")
    val queryDate = format.parseDateTime("2022-01-01")

    val ticketsInfo = cats.ticketsInfo(showDate, queryDate)
    ticketsInfo.ticketsLeft shouldEqual 200
    ticketsInfo.ticketsAvailable shouldEqual 0
  }


  "Show price" should "be taking fixed to genre price" in {
    show calculatePrice startDate shouldEqual Drama.price
    comedy calculatePrice startDate shouldEqual Comedy.price
    musical calculatePrice startDate shouldEqual Musical.price
  }

  "Show price" should "be reduced after 80 days" in {
    show calculatePrice(startDate plusDays  81) shouldEqual Drama.price * 0.8
    comedy calculatePrice(startDate plusDays 81) shouldEqual Comedy.price * 0.8
    musical calculatePrice(startDate plusDays 81) shouldEqual Musical.price * 0.8
  }

}
